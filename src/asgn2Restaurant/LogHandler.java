package asgn2Restaurant;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;
import asgn2Customers.Customer;
import asgn2Customers.CustomerFactory;
import asgn2Exceptions.CustomerException;
import asgn2Exceptions.LogHandlerException;
import asgn2Exceptions.PizzaException;
import asgn2Pizzas.Pizza;
import asgn2Pizzas.PizzaFactory;

/**
 *
 * A class that contains methods that use the information in the log file to return Pizza 
 * and Customer object - either as an individual Pizza/Customer object or as an
 * ArrayList of Pizza/Customer objects.
 * 
 * @author David Nguyen and Kodee Lund
 *
 */
public class LogHandler {
	
	/**
	 * Returns an ArrayList of Customer objects from the information contained in the log file ordered as they appear in the log file.
	 * @param filename The file name of the log file
	 * @return an ArrayList of Customer objects from the information contained in the log file ordered as they appear in the log file. 
	 * @throws CustomerException If the log file contains semantic errors leading that violate the customer constraints listed in Section 5.3 of the Assignment Specification or contain an invalid customer code (passed by another class).
	 * @throws LogHandlerException If there was a problem with the log file not related to the semantic errors above such as not being able to load a file.
	 * 
	 */
	public static ArrayList<Customer> populateCustomerDataset(String filename) throws CustomerException, LogHandlerException{
		ArrayList<Customer> customerDataset = new ArrayList<Customer>();
		
		BufferedReader bufferedReader;

			try {
				bufferedReader= new BufferedReader(new FileReader(filename));
				String readLine;
				while ((readLine = bufferedReader.readLine()) != null) {
					customerDataset.add(createCustomer(readLine));
				}
				bufferedReader.close();
			} catch (IOException e) {
				throw new LogHandlerException("Failed to load customer data due to problems with log file.");
			}

		return customerDataset;
	}		

	/**
	 * Returns an ArrayList of Pizza objects from the information contained in the log file ordered as they appear in the log file. .
	 * @param filename The file name of the log file
	 * @return an ArrayList of Pizza objects from the information contained in the log file ordered as they appear in the log file. .
	 * @throws PizzaException If the log file contains semantic errors leading that violate the pizza constraints listed in Section 5.3 of the Assignment Specification or contain an invalid pizza code (passed by another class).
	 * @throws LogHandlerException If there was a problem with the log file not related to the semantic errors above such as failing to load a file
	 * 
	 */
	public static ArrayList<Pizza> populatePizzaDataset(String filename) throws PizzaException, LogHandlerException{
		ArrayList<Pizza> pizzaDataset = new ArrayList<Pizza>();
		BufferedReader bufferedReader;

			try {
				bufferedReader= new BufferedReader(new FileReader(filename));
				String readLine;
				while ((readLine = bufferedReader.readLine()) != null) {
					pizzaDataset.add(createPizza(readLine));
				}
				bufferedReader.close();
			} catch (IOException e) {
				throw new LogHandlerException("Failed to load pizza data due to problems with log file.");
			}

		return pizzaDataset;
	}		

	
	/**
	 * Creates a Customer object by parsing the  information contained in a single line of the log file. The format of 
	 * each line is outlined in Section 5.3 of the Assignment Specification.  
	 * @param line - A line from the log file
	 * @return- A Customer object containing the information from the line in the log file
	 * @throws CustomerException - If the log file contains semantic errors leading that violate the customer constraints listed in Section 5.3 of the Assignment Specification or contain an invalid customer code (passed by another class).
	 * @throws LogHandlerException - If mobile number contains letters; locations x and y are characters and not numbers.
	 */
	public static Customer createCustomer(String line) throws CustomerException, LogHandlerException{
		final int namePosition = 2;
		final int mobileNumberPosition = 3;
		final int typePosition = 4;
		final int locX = 5;
		final int locY = 6;
		final int numOfData = 9;
		String[] logData;
		Customer customer;
		
		logData = line.split(",");
		
		// Checks for when log data contains too little or too much data
		if (logData.length < numOfData || logData.length > numOfData) {
			throw new LogHandlerException("Insufficient data in logs or log file is invalid type.");
		} else {
			// If any problems occur while parsing data, throw new exception message
			try {
				int locationX, locationY;
				String name = logData[namePosition];
				String mobileNumber = logData[mobileNumberPosition];
				
				try {
					locationX = Integer.parseInt(logData[locX]);
				} catch (Exception e) {
					throw new LogHandlerException("X-Coordinate is invalid.");
				}
				try {
					locationY = Integer.parseInt(logData[locY]);
				} catch (Exception e) {
					throw new LogHandlerException("X-Coordinate is invalid.");
				}
				String customerType = logData[typePosition];
						
				customer = CustomerFactory.getCustomer(customerType, name, mobileNumber, locationX, locationY);
			} catch (Exception e) {
				throw new LogHandlerException("Data in log file line: \n" + line + "\nContains an error due to: \n" + e.getMessage());
			}
		}		
		return customer;
	}
	
	/**
	 * Creates a Pizza object by parsing the information contained in a single line of the log file. The format of 
	 * each line is outlined in Section 5.3 of the Assignment Specification.  
	 * @param line - A line from the log file
	 * @return- A Pizza object containing the information from the line in the log file
	 * @throws PizzaException If the log file contains semantic errors leading that violate the pizza constraints listed in Section 5.3 of the Assignment Specification or contain an invalid pizza code (passed by another class).
	 * @throws LogHandlerException - If number of pizza data is invalid; order or deliver times are incorrect format.
	 */
	public static Pizza createPizza(String line) throws PizzaException, LogHandlerException{
		// Positions of the required data in a line of string to create a pizza
		final int orderTimePosition = 0;
		final int deliveryTimePosition = 1;
		final int pizzaCodePosition = 7;
		final int pizzaQuantityPosition = 8;
		final int numOfData = 9;
		
		String[] logData;
		Pizza pizza;
		
		logData = line.split(",");
		
		// Checks for when log data contains too little or too much data
		if (logData.length < numOfData || logData.length > numOfData) {
			throw new LogHandlerException("Insufficient data in logs or log file is invalid type.");
		} else {
			// If any problems occur while parsing data, throw new exception message
			try {
				int numOfPizzas;
				LocalTime orderTime;
				LocalTime deliveryTime;
				String pizzaCode;
				
				try {
					numOfPizzas = Integer.parseInt(logData[pizzaQuantityPosition]);
				} catch (Exception e) {
					throw new LogHandlerException("Number of pizza data is invalid.");
				}
				
				try {
					orderTime = LocalTime.parse(logData[orderTimePosition]);
				} catch (Exception e) {
					throw new LogHandlerException("Order time data is invalid format.");
				}
				
				try {
					deliveryTime = LocalTime.parse(logData[deliveryTimePosition]);
				} catch (Exception e) {
					throw new LogHandlerException("Delivery time data is invalid format.");
				}
				
				pizzaCode = logData[pizzaCodePosition];
				
				pizza = PizzaFactory.getPizza(pizzaCode, numOfPizzas, orderTime, deliveryTime);
			} catch (Exception e) {
				throw new LogHandlerException("Data in log file line: \n" + line + "\nContains an error due to: \n" + e.getMessage());
			}
		}		
		return pizza;
	}

}
