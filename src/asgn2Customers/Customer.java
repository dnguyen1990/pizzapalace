package asgn2Customers;

import asgn2Exceptions.CustomerException;

/** An abstract class to represent a customer at the Pizza Palace restaurant.
 *  The Customer class is used as a base class of PickUpCustomer, 
 *  DriverDeliveryCustomer and DroneDeliverCustomer. Each of these subclasses overwrites
 *  the abstract method getDeliveryDistance. A description of the class's
 * fields and their constraints is provided in Section 5.2 of the Assignment Specification.  
 * 
 * @author Kodee Lund
*/
public abstract class Customer {

	String custName, custMobileNumber, custType;
	int locX, locY;
	final int minLength = 1;
	final int maxLength = 20;
	final int ten = 10;
	final int posTenBlocks = 10;
	final int negTenBlocks = -10;
	
	/**
	 *  This class represents a customer of the Pizza Palace restaurant.  A detailed description of the class's fields
	 *  and parameters is provided in the Assignment Specification, in particular in Section 5.2. 
	 *  A CustomerException is thrown if the any of the constraints listed in Section 5.2 of the Assignment Specification
	 *  are violated. 
	 *  
  	 * <P> PRE: True
  	 * <P> POST: All field values are set
  	 * 
	 * @param name - The Customer's name 
	 * @param mobileNumber - The customer mobile number
	 * @param locationX - The customer x location relative to the Pizza Palace Restaurant measured in units of 'blocks' 
	 * @param locationY - The customer y location relative to the Pizza Palace Restaurant measured in units of 'blocks' 
	 * @param type - A human understandable description of this Customer type
	 * @throws CustomerException if customer name is less than 1 character or greater than 20 characters long; if name is empty or null or contains white space;
	 * if mobile number is not numbers, too short or too long, null; if delivery coordinates are greater than 10 in any direction; contains invalid custoemr types
	 * 
	 */
	public Customer(String name, String mobileNumber, int locationX, int locationY, String type) throws CustomerException{
		if (name.length() < minLength){
			// Throws Exception for blank name
			throw new CustomerException("Name needs to be filled out");
		} if (name.length() > maxLength){
			// Throws Exception for name being too long
			throw new CustomerException("Name is too long");
		} if (name == "" || name == null){
			// Throws Exception for null or empty name
			throw new CustomerException("Name is either empty or null");
		} if (mobileNumber.length() < minLength){
			// Throws Exception for empty mobile number
			throw new CustomerException("Number needs to be filled out");
		} if (mobileNumber == "" || mobileNumber == null) {
			// Throws Exception for empty or null mobile number
			throw new CustomerException("mobileNumber is either null or empty");
		} if (mobileNumber.length() != ten) {
			// Throws Exception for invalid length of mobile number
			throw new CustomerException("Number is too long or too short");
		} if (!mobileNumber.substring(0, 1).equals("0")){
			// Throws Exception for invalid first digit for mobile number
			throw new CustomerException("Number is invalid, must start with a 0");
		} if (locationX < negTenBlocks) {
			// Throws Exception for being over max delivery distance west
			throw new CustomerException("Cannot deliver more than 10 blocks west");
		} if (locationX > posTenBlocks) {
			// Throws Exception for being over max delivery distance east
			throw new CustomerException("Cannot deliver more than 10 blocks east");
		} if (locationY < negTenBlocks) {
			// Throws Exception for being over max delivery distance south
			throw new CustomerException("Cannot deliver more than 10 blocks south");
		} if (locationY > posTenBlocks) {
			// Throws Exception for being over max delivery distance north
			throw new CustomerException("Cannot deliver more than 10 blocks north");
		} if (!type.equals("Pick Up") && !type.equals("Drone Delivery") && !type.equals("Driver Delivery")) {
			// Throws Exception for invalid delivery type
			throw new CustomerException("Customer type is not any valid type");
		} if (locationX == 0 && locationY == 0 && !type.equals("Pick Up")) {
			// Throws Exception for delivery to store
			throw new CustomerException("Customer order cannot be delivered to store");
		} if (locationX != 0 && locationY != 0 && type.equals("Pick Up")) {
			// Throws Exception for pickUp while not at store
			throw new CustomerException("Customer type cannot be PickUp while distance is not 0");
		} if (locationX == 0 && locationY == 0 && type.equals("Drone Delviery")) {
			// Throws Exception for drone delivery to store is invalid
			throw new CustomerException("Customer type cannot be Drone Delivery while distance is 0");
		} if (locationX == 0 && locationY == 0 && type.equals("Driver Delivery")) {
			// Throws Exception for Driver delivery to store is invalid
			throw new CustomerException("Customer type cannot be Driver Delivery while distance is 0");
		} if (!mobileNumber.matches("[0-9]+")) {
			// Throws Exception for mobile Number not containing only digits
			throw new CustomerException("Mobile number provided contains invalid characters.");
		} if (name.matches("[ ]+")) {
			// Throws Exception for name with only spaces
			throw new CustomerException("Customer's name contains only white space.");
		} else {
			this.custName = name;
			this.custMobileNumber = mobileNumber;
			this.locX = locationX;
			this.locY = locationY;
			this.custType = type;
		}
	}
	
	/**
	 * Returns the Customer's name.
	 * @return The Customer's name.
	 */
	public final String getName(){
		return this.custName;
	}
	
	/**
	 * Returns the Customer's mobile number.
	 * @return The Customer's mobile number.
	 */
	public final String getMobileNumber(){
		return this.custMobileNumber;
	}

	/**
	 * Returns a human understandable description of the Customer's type. 
	 * The valid alternatives are listed in Section 5.2 of the Assignment Specification. 
	 * @return A human understandable description of the Customer's type.
	 */
	public final String getCustomerType(){
		return this.custType;
	}
	
	/**
	 * Returns the Customer's X location which is the number of blocks East or West 
	 * that the Customer is located relative to the Pizza Palace restaurant. 
	 * @return The Customer's X location
	 */
	public final int getLocationX(){
		return this.locX;
	}

	/**
	 * Returns the Customer's Y location which is the number of blocks North or South 
	 * that the Customer is located relative to the Pizza Palace restaurant. 
	 * @return The Customer's Y location
	 */
	public final int getLocationY(){
		return this.locY;
	}

	/**
	 * An abstract method that returns the distance between the Customer and 
	 * the restaurant depending on the mode of delivery. 
	 * @return The distance between the restaurant and the Customer depending on the mode of delivery.
	 */
	public abstract double getDeliveryDistance();

	
	
	/**
	 * Compares *this* Customer object with an instance of an *other* Customer object and returns true if  
	 * if the two objects are equivalent, that is, if the values exposed by public methods are equal.
	 *  You do not need to test this method.
	 * 
	 * @return true if *this* Customer object and the *other* Customer object have the same values returned for 	
	 * getName(),getMobileNumber(),getLocationX(),getLocationY(),getCustomerType().
	 */
	@Override
	public boolean equals(Object other){
		Customer otherCustomer = (Customer) other;

		return ( (this.getName().equals(otherCustomer.getName()))  &&
			(this.getMobileNumber().equals(otherCustomer.getMobileNumber())) && 
			(this.getLocationX() == otherCustomer.getLocationX()) && 
			(this.getLocationY() == otherCustomer.getLocationY()) && 
			(this.getCustomerType().equals(otherCustomer.getCustomerType())) );			
	}
}
