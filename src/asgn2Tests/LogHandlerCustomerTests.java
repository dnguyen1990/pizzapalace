package asgn2Tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import asgn2Customers.Customer;
import asgn2Customers.CustomerFactory;
import asgn2Exceptions.CustomerException;
import asgn2Exceptions.LogHandlerException;
import asgn2Restaurant.LogHandler;

/**
 * A class that tests the methods relating to the creation of Customer objects in the asgn2Restaurant.LogHander class.
 *
 * @author David Nguyen
 */
public class LogHandlerCustomerTests {
	// TO DO
	ArrayList<Customer> customersTest = new ArrayList<Customer>();
	
	@Before
	// Setup a test array list of customers
	public void SetupLog() throws CustomerException, LogHandlerException {
		customersTest = LogHandler.populateCustomerDataset("./logs/20170101.txt");
	}
	
	@Test (expected = LogHandlerException.class)
	// Test to see that a log file with customer name errors in it will throw an exception
	public void ErrorLogName() throws CustomerException, LogHandlerException {
		customersTest = LogHandler.populateCustomerDataset("./logs/test1.txt");
	}
	
	@Test (expected = LogHandlerException.class)
	// Test to see that a log file with delivery code errors in it will throw an exception
	public void ErrorLogCode() throws CustomerException, LogHandlerException {
		customersTest = LogHandler.populateCustomerDataset("./logs/test2.txt");
	}
	
	@Test (expected = LogHandlerException.class)
	// Test to see that a log file with coordinates errors in it will throw an exception
	public void ErrorLogCoordinates() throws CustomerException, LogHandlerException {
		customersTest = LogHandler.populateCustomerDataset("./logs/invalidcoord.txt");
	}
	
	@Test (expected = LogHandlerException.class)
	// Test to see that a log file with mobile number errors in it will throw an exception
	public void ErrorLogMobileNumber() throws CustomerException, LogHandlerException {
		customersTest = LogHandler.populateCustomerDataset("./logs/invalidmob.txt");
	}
	
	@Test
	// Test getting first customer in log file shown above
	public void testGetFirstCustomer() throws CustomerException {
		final int firstCustomer = 0;
		Customer customerToCompare = (Customer) CustomerFactory.getCustomer("DVC", "Casey Jones", "0123456789", 5, 5); // First customer as seen in log
		assertTrue(customersTest.get(firstCustomer).equals(customerToCompare));
	}
	
	@Test
	// Test getting last customer in log file shown above
	public void testGetLastCustomer() throws CustomerException {
		int lastCustomer = customersTest.size() - 1;
		Customer customerToCompare = (Customer) CustomerFactory.getCustomer("PUC", "Oroku Saki", "0111222333", 0, 0); // Last customer as seen in log
		assertTrue(customersTest.get(lastCustomer).equals(customerToCompare));
	}
	
	// Tests for functionality in create customer method
	
	@Test
	// Test that create customer method works with a valid line
	public void testValidString() throws CustomerException, LogHandlerException {
		final String validLine = "19:10:00,19:20:00,Casey Jones,0323456789,DVC,5,5,PZL,2";
		LogHandler.createCustomer(validLine);
	}
	
	// Tests for exception handling in create customer method
	
	@Test (expected = LogHandlerException.class)
	// Test that exception is thrown if line of string does not contain 9 elements of data
	public void testInsufficientData() throws LogHandlerException, CustomerException {
		final String invalidLine = "19:10:00,19:20:00,Casey Jones,0623456789,DVC,5,5,PZL";
		LogHandler.createCustomer(invalidLine);
	}
	
	@Test (expected = LogHandlerException.class)
	// Test that exception is thrown if line of string contains more than 9 elements of data
	public void testTooMuchData() throws LogHandlerException, CustomerException {
		final String invalidLine = "19:10:00,19:20:00,Casey Jones,0623456789,DVC,5,5,PZL,2,4";
		LogHandler.createCustomer(invalidLine);
	}
	
	@Test (expected = LogHandlerException.class)
	// Test that exception is thrown if mobile number if invalid
	public void testExceptionForInvalidNumber() throws LogHandlerException, CustomerException {
		final String invalidNumber = "19:10:00,19:20:00,Casey Jones,0A23456789,DVC,5,5,PZL,2";
		LogHandler.createCustomer(invalidNumber);
	}
	
	@Test (expected = LogHandlerException.class)
	// Test that exception is thrown if invalid format for Location X
	public void testExceptionInvalidLocationX() throws CustomerException, LogHandlerException {
		final String invalidLocationX = "19:00:00,19:20:00,Casey Jones,0123456789,DVC,F,5,PZV,2";
		LogHandler.createCustomer(invalidLocationX);
	}
	
	@Test (expected = LogHandlerException.class)
	// Test that exception is thrown if invalid format for Location y
	public void testExceptionInvalidLocationY() throws CustomerException, LogHandlerException {
		final String invalidLocationY = "19:10:00,19:20:00,Casey Jones,0223456789,DVC,5,G,PZL,2";
		LogHandler.createCustomer(invalidLocationY);
	}
	
	@Test (expected = LogHandlerException.class)
	//Test for invalid name. I.e name cannot have all white space
	public void testInvalidName() throws CustomerException, LogHandlerException {
		final String invalidName ="19:00:00,19:20:00,   ,0123456789,DVC,5,5,PZV,2";
		LogHandler.createCustomer(invalidName);
	}
	
	@Test (expected = LogHandlerException.class)
	//Test for invalid type. Customer code must be in letters
	public void testInvalidType() throws CustomerException, LogHandlerException {
		final String invalidType ="19:10:00,19:20:00,Casey Jones,0223456789,DV4,5,4,PZL,2";
		LogHandler.createCustomer(invalidType);
	}
}
