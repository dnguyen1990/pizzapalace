package asgn2Tests;

import static org.junit.Assert.*;
import java.time.LocalTime;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;


import asgn2Pizzas.Pizza;
import asgn2Pizzas.PizzaFactory;
import asgn2Exceptions.PizzaException;
import asgn2Exceptions.LogHandlerException;
import asgn2Restaurant.LogHandler;

/** A class that tests the methods relating to the creation of Pizza objects in the asgn2Restaurant.LogHander class.
* 
* @author Kodee Lund
* 
*/
public class LogHandlerPizzaTests {
	ArrayList<Pizza> pizzasTest = new ArrayList<Pizza>();
	
	@Before
	// Setup a test array list Pizzas
	public void SetupLog() throws PizzaException, LogHandlerException {
		pizzasTest = LogHandler.populatePizzaDataset("./logs/20170101.txt");
	}
	
	@Test (expected = LogHandlerException.class)
	// Test exception is thrown when log contains pizza code errors
	public void ErrorLogPizzaCode() throws PizzaException, LogHandlerException {
		pizzasTest = LogHandler.populatePizzaDataset("./logs/invalidpizza.txt");
	}
	
	@Test (expected = LogHandlerException.class)
	// Test exception is thrown when log contains quantity errors
	public void ErrorLogQuantity() throws PizzaException, LogHandlerException {
		pizzasTest = LogHandler.populatePizzaDataset("./logs/invalidquantity.txt");
	}
	
	@Test (expected = LogHandlerException.class)
	// Test exception is thrown when log contains order time parsing errors
	public void ErrorLogOrderTimeError() throws PizzaException, LogHandlerException {
		pizzasTest = LogHandler.populatePizzaDataset("./logs/invalidtime.txt");
	}
	
	@Test (expected = LogHandlerException.class)
	// Test exception is thrown when log contains order time parsing errors
	public void ErrorLogDeliveryTimeError() throws PizzaException, LogHandlerException {
		pizzasTest = LogHandler.populatePizzaDataset("./logs/invaliddeltime.txt");
	}
	
	@Test
	// Tests first Pizza shown
	public void testFirstPizza() throws PizzaException {
		final int firstPizza = 0;
		Pizza pizzaToCompare = (Pizza) PizzaFactory.getPizza("PZV", 2, LocalTime.parse("19:00:00"), LocalTime.parse("19:20:00")); // Data for first pizza in log
		assertTrue(pizzasTest.get(firstPizza).equals(pizzaToCompare));
	}
	@Test
	// Test last Pizza shown
	public void testGetLastPizza() throws PizzaException {
		int lastPizza = pizzasTest.size() - 1;
		Pizza pizzaToCompare = (Pizza) PizzaFactory.getPizza("PZL", 3, LocalTime.parse("21:00:00"), LocalTime.parse("21:35:00")); // Data for last pizza in log
		assertTrue(pizzasTest.get(lastPizza).equals(pizzaToCompare));
	}
	
	// Tests Pizza Create functionality 
	
	@Test 
	// Tests create Valid Pizza 
	public void testValidPizza() throws PizzaException, LogHandlerException {
		final String validInfo = "19:00:00,19:20:00,Casey Jones,0123456789,DVC,5,5,PZV,2";
		LogHandler.createPizza(validInfo);
	}
	
	// Tests Exceptions Pizza Create 
	@Test (expected = LogHandlerException.class)
	// Tests if more than 9 elements info 
	public void testInsufficientData() throws LogHandlerException, PizzaException {
		final String invalidLine = "20:00:00,20:25:00,April O'Neal,0987654321,DNC,3,4,PZM,1,2";
		LogHandler.createPizza(invalidLine);
	}
	
	@Test (expected = LogHandlerException.class)
	// Tests if less than 9 elements info
	public void testExcessiveData() throws LogHandlerException, PizzaException {
		final String invalidLine = "20:00:00,20:25:00,April O'Neal,0987654321,DNC,3,4,PZM";
		LogHandler.createPizza(invalidLine);
	}
	
	@Test (expected = LogHandlerException.class)
	// Tests invalid orderTime format
	public void testOrderTimeFormat() throws LogHandlerException, PizzaException {
		final String invalidLine = "20:00:00:00,20:25:00,April O'Neal,0987654321,DNC,3,4,PZM,1";
		LogHandler.createPizza(invalidLine);
	}
	
	@Test (expected = LogHandlerException.class)
	// Tests invalid deliveryTime format
	public void testDeliveryTimeFormat() throws LogHandlerException, PizzaException {
		final String invalidLine = "20:00:00,20:25:00:00,April O'Neal,0987654321,DNC,3,4,PZM,1";
		LogHandler.createPizza(invalidLine);
	}
	
	@Test (expected = LogHandlerException.class)
	// Tests invalid PizzaType
	public void testPizzaType() throws LogHandlerException, PizzaException {
		final String invalidLine = "20:00:00,20:25:00,April O'Neal,0987654321,DNC,3,4,6ZM,1";
		LogHandler.createPizza(invalidLine);
	}
	
	@Test (expected = LogHandlerException.class)
	// Tests invalid Quantity format. I.e. cannot be a letter, must be number
	public void testQuantity() throws LogHandlerException, PizzaException {
		final String invalidLine = "20:00:00,20:25:00,April O'Neal,0987654321,DSC,3,4,PZM,F";
		LogHandler.createPizza(invalidLine);
	}
}
