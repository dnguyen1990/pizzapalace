package asgn2Tests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import asgn2Customers.Customer;
import asgn2Customers.CustomerFactory;
import asgn2Customers.DriverDeliveryCustomer;
import asgn2Customers.DroneDeliveryCustomer;
import asgn2Customers.PickUpCustomer;
import asgn2Exceptions.CustomerException;

/**
 * A class the that tests the asgn2Customers.CustomerFactory class.
 * 
 * @author David Nguyen
 *
 */
public class CustomerFactoryTests {
	// TO DO
	
	final String pickUp = "PUC";
	final String drone = "DNC";
	final String driver = "DVC";
	final String validMobileNumber = "0123456789";
	final int zero = 0;
	final int location = 2;
	final String validName = "Dave";
	
	@Test
	// Test creating a pick up customer
	public void testGetPickUpCustomer() throws CustomerException {
		Customer customer = (Customer) CustomerFactory.getCustomer(pickUp, validName, validMobileNumber, zero, zero);
		assertTrue(customer.getCustomerType().equals("Pick Up"));
	}
	
	@Test
	// Test creating a drone delivery customer
	public void testGetDroneCustomer() throws CustomerException {
		Customer customer = (Customer) CustomerFactory.getCustomer(drone, validName, validMobileNumber, location, location);
		assertTrue(customer.getCustomerType().equals("Drone Delivery"));
	}
	
	@Test
	// Test creating a drone delivery customer
	public void testGetDriverCustomer() throws CustomerException {
		Customer customer = (Customer) CustomerFactory.getCustomer(driver, validName, validMobileNumber, location, location);
		assertTrue(customer.getCustomerType().equals("Driver Delivery"));
	}
	
	@Test (expected = CustomerException.class)
	// Test that exception occurs when code does not match
	public void testIncorrectCustomerCode() throws CustomerException {
		final String fakeCode = "PEW";
		Customer customer = (Customer) CustomerFactory.getCustomer(fakeCode, validName, validMobileNumber, zero, zero);
	}
	
}
