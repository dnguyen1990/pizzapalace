package asgn2Tests;
import static org.junit.Assert.*;

import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import asgn2Pizzas.Pizza;
import asgn2Pizzas.PizzaFactory;
import asgn2Exceptions.CustomerException;
import asgn2Exceptions.LogHandlerException;
import asgn2Exceptions.PizzaException;
import asgn2Restaurant.PizzaRestaurant;

/**
 * A class that tests the methods relating to the handling of Pizza objects in the asgn2Restaurant.PizzaRestaurant class as well as
 * processLog and resetDetails.
 * 
 * @author Kodee Lund
 *
 */
public class RestaurantPizzaTests {
	
	PizzaRestaurant restaurant = new PizzaRestaurant();
	
	@Before
	// Set up a pizza restaurant using valid log file
	public void setupRestaurant() throws CustomerException, PizzaException, LogHandlerException {
		restaurant.processLog("./logs/20170101.txt");
	}
	
	// General Tests
	
	@Test
	// Tests number of pizza orders
	public void testNumberPizzaOrders() {
		final int pizzas = 3;
		assertEquals(pizzas, restaurant.getNumPizzaOrders());
	}
	@Test
	//Test total Profit
	public void testTotalProfit() {
		final double totalProfit = 36.5;
		assertTrue(totalProfit == restaurant.getTotalProfit());
	}
	@Test (expected = IndexOutOfBoundsException.class)
	// Tests reset details
	public void testResetValues() throws PizzaException {
		final int firstPizza = 0;
		restaurant.resetDetails();
		restaurant.getPizzaByIndex(firstPizza);
	}
	
	// Tests for Pizza Index 
	
	@Test 
	// Tests Get Pizza by index (1st Pizza)
	public void testPizzaIndexFirst() throws PizzaException {
		final int firstPizza = 0;
		Pizza pizza = (Pizza) PizzaFactory.getPizza("PZV", 2, LocalTime.parse("19:00:00"), LocalTime.parse("19:20:00"));
		restaurant.getPizzaByIndex(firstPizza);
	}
	@Test 
	// Tests Get Pizza by index (last Pizza)
	public void testPizzaIndexLast() throws PizzaException {
		final int lastPizza = 2;
		Pizza pizza = (Pizza) PizzaFactory.getPizza("PZL", 3, LocalTime.parse("21:00:00"), LocalTime.parse("21:35:00"));
		restaurant.getPizzaByIndex(lastPizza);
	}
	
	// Tests for exceptions Pizza Index
	
	@Test (expected = PizzaException.class)
	// Tests Get Pizza by index (1st Pizza)
	public void testPizzaIndexBelowValid() throws PizzaException {
		final int belowValidPizza = -1;
		restaurant.getPizzaByIndex(belowValidPizza);
	}
	@Test (expected = PizzaException.class)
	// Tests Get Pizza by index (last Pizza)
	public void testPizzaIndexAboveValid() throws PizzaException {
		final int aboveValidPizza = restaurant.getNumPizzaOrders() + 1;
		restaurant.getPizzaByIndex(aboveValidPizza);
	}
	
}
