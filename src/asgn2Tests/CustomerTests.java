package asgn2Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import asgn2Customers.Customer;
import asgn2Customers.CustomerFactory;
import asgn2Customers.DriverDeliveryCustomer;
import asgn2Customers.DroneDeliveryCustomer;
import asgn2Customers.PickUpCustomer;
import asgn2Exceptions.CustomerException;

/**
 * A class that tests the that tests the asgn2Customers.PickUpCustomer, asgn2Customers.DriverDeliveryCustomer,
 * asgn2Customers.DroneDeliveryCustomer classes. Note that an instance of asgn2Customers.DriverDeliveryCustomer 
 * should be used to test the functionality of the  asgn2Customers.Customer abstract class. 
 * 
 * @author David Nguyen
 * 
 *
 */
public class CustomerTests {
	// TO DO
	
	final String pickUp = "PUC";
	final String drone = "DNC";
	final String driver = "DVC";
	final String validName = "Testing Name";
	final String validMobileNumber = "0123456789";
	final int zero = 0;
	final int distance1 = 4;
	final int distance2 = -5;
	Customer testCustomer1;
	Customer testCustomer2;
	Customer testCustomer3;
	
	@Before
	// Set up a test pick up customer, one for each type of customer (pick up, drone and driver)
	public void setUpCustomers() throws CustomerException {	
		testCustomer1 = new PickUpCustomer(validName, validMobileNumber, zero, zero);
		testCustomer2 = new DroneDeliveryCustomer(validName, validMobileNumber, distance1, distance2);
		testCustomer3 = new DriverDeliveryCustomer(validName, validMobileNumber, distance1, distance2);
	}
	
	// Pick Customers
	
	@Test
	// Test that name returns correctly for pick up
	public void testGetNameForPickUp() {
		assertTrue(testCustomer1.getName().equals(validName));
	}
	
	@Test
	// Test that mobile number returns correctly for pick up
	public void testGetMobileNumberForPickUp() {
		assertTrue(testCustomer1.getMobileNumber().equals(validMobileNumber));
	}
	
	@Test
	// Test that customer type returns correctly for pick up
	public void testGetCustomerTypeForPickUp() {
		assertTrue(testCustomer1.getCustomerType().equals("Pick Up"));
	}
	
	@Test
	// Test that customer location x returns correctly for pick up
	public void testGetLocationXForPickUp() {
		assertTrue(testCustomer1.getLocationX() == zero);
	}
	
	@Test
	// Test that customer location x returns correctly for pick up
	public void testGetLocationYForPickUp() {
		assertTrue(testCustomer1.getLocationY() == zero);
	}
	
	@Test
	// Test that customer location x returns correctly for pick up
	public void testGetDistanceForPickUp() {
		assertTrue(testCustomer1.getDeliveryDistance() == zero);
	}
	
	// Drone Customers
	
	@Test
	// Test that name returns correctly for drone
	public void testGetNameForDrone() {
		assertTrue(testCustomer2.getName().equals(validName));
	}
	
	@Test
	// Test that mobile number returns correctly for drone
	public void testGetMobileNumberForDrone() {
		assertTrue(testCustomer2.getMobileNumber().equals(validMobileNumber));
	}
	
	@Test
	// Test that customer type returns correctly for drone
	public void testGetCustomerTypeForDrone() {
		assertTrue(testCustomer2.getCustomerType().equals("Drone Delivery"));
	}
	
	@Test
	// Test that customer location x returns correctly for drone
	public void testGetLocationXForDrone() {
		assertTrue(testCustomer2.getLocationX() == distance1);
	}
	
	@Test
	// Test that customer location x returns correctly for drone
	public void testGetLocationYForDrone() {
		assertTrue(testCustomer2.getLocationY() == distance2);
	}
	
	@Test
	// Test that customer location x returns correctly for drone
	public void testGetDistanceForDrone() {
		Double distance = Math.sqrt(distance1 * distance1 + distance2 * distance2);
		assertTrue(testCustomer2.getDeliveryDistance() == distance);
	}
	
	@Test
	// Test that a drone customer can order with a coordinate with a 0 in the x
	public void testDeliveryCoordinatesDroneX() throws CustomerException {
		final int blocks = 5;
		Customer customer = new DroneDeliveryCustomer(validName, validMobileNumber, zero, blocks);
	}
	
	@Test
	// Test that a drone customer can order with a coordinate with a 0 in the y
	public void testDeliveryCoordinatesDroneY() throws CustomerException {
		final int blocks = 5;
		Customer customer = new DroneDeliveryCustomer(validName, validMobileNumber, blocks, zero);
	}
	
	// Driver Customers
	
	@Test
	// Test that name returns correctly for driver
	public void testGetNameForDriver() {
		assertTrue(testCustomer3.getName().equals(validName));
	}
	
	@Test
	// Test that mobile number returns correctly for Driver
	public void testGetMobileNumberForDriver() {
		assertTrue(testCustomer3.getMobileNumber().equals(validMobileNumber));
	}
	
	@Test
	// Test that customer type returns correctly for Driver
	public void testGetCustomerTypeForDriver() {
		assertTrue(testCustomer3.getCustomerType().equals("Driver Delivery"));
	}
	
	@Test
	// Test that customer location x returns correctly for Driver
	public void testGetLocationXForDriver() {
		assertTrue(testCustomer3.getLocationX() == distance1);
	}
	
	@Test
	// Test that customer location x returns correctly for Driver
	public void testGetLocationYForDriver() {
		assertTrue(testCustomer3.getLocationY() == distance2);
	}
	
	@Test
	// Test that customer location x returns correctly for Driver
	public void testGetDistanceForDriver() {
		int distance = (Math.abs(distance1) + Math.abs(distance2));
		assertTrue(testCustomer3.getDeliveryDistance() == distance);
	}
	
	@Test
	// Test that a driver customer can order with a coordinate with a 0 in the x
	public void testDeliveryCoordinatesDriverX() throws CustomerException {
		final int blocks = 5;
		Customer customer = new DriverDeliveryCustomer(validName, validMobileNumber, zero, blocks);
	}
	
	@Test
	// Test that a driver customer can order with a coordinate with a 0 in the x
	public void testDeliveryCoordinatesDriverY() throws CustomerException {
		final int blocks = 5;
		Customer customer = new DriverDeliveryCustomer(validName, validMobileNumber, blocks, zero);
	}
	
	// Testing Exceptions in customer class
	
	@Test (expected = CustomerException.class)
	// Test that customer's mobile number throws a exception if too long
	public void testMobNumberTooLong() throws CustomerException {
		final String invalidNumber = "012345678910";
		Customer customer = new DriverDeliveryCustomer(validName, invalidNumber, distance1, distance2);
	}
	
	@Test (expected = CustomerException.class)
	// Test that customer's mobile number throws a exception if too short
	public void testMobNumberTooShort() throws CustomerException {
		final String invalidNumber = "012345678";
		Customer customer = new DriverDeliveryCustomer(validName, invalidNumber, distance1, distance2);
	}
	
	@Test (expected = CustomerException.class)
	// Test that customer's mobile number throws a exception if too short
	public void testMobNumberWithoutLeadingZero() throws CustomerException {
		final String invalidNumber = "1234567890";
		Customer customer = new DriverDeliveryCustomer(validName, invalidNumber, distance1, distance2);
	}
	
	@Test (expected = CustomerException.class)
	// Test that customer's name throws a exception if its empty or too short
	public void testNameTooShort() throws CustomerException {
		final String invalidName = "";
		Customer customer = new DriverDeliveryCustomer(invalidName, validMobileNumber, distance1, distance2);
	}
	
	@Test (expected = CustomerException.class)
	// Test that customer's name throws a exception if its too long
	public void testNameTooLong() throws CustomerException {
		final String invalidName = "This is a really long string for a name for testing";
		Customer customer = new DriverDeliveryCustomer(invalidName, validMobileNumber, distance1, distance2);
	}
	
	@Test (expected = CustomerException.class)
	// Test that exception is thrown if a pick up customer has invalid location data (i.e it should be zero)
	public void testLocationForPickUp() throws CustomerException {
		Customer customer = new PickUpCustomer(validName, validMobileNumber, distance1, distance2);
	}
	
	@Test (expected = CustomerException.class)
	// Test that exception is thrown if a drone customer has invalid location data (i.e it should not be at the restaurant)
	public void testLocationForDrone() throws CustomerException {
		Customer customer = new DroneDeliveryCustomer(validName, validMobileNumber, zero, zero);
	}
	
	@Test (expected = CustomerException.class)
	// Test that exception is thrown if a drone customer has invalid location data (i.e it should not be at the restaurant)
	public void testLocationForDriver() throws CustomerException {
		Customer customer = new DriverDeliveryCustomer(validName, validMobileNumber, zero, zero);
	}
	
	@Test (expected = CustomerException.class)
	// Test that exception is thrown if delivery order is placed more than 10 blocks north
	public void testLocation10BlocksNorth() throws CustomerException {
		final int blocks = 11;
		Customer customer = new DroneDeliveryCustomer(validName, validMobileNumber, zero, blocks);
	}
	
	@Test (expected = CustomerException.class)
	// Test that exception is thrown if delivery order is placed more than 10 blocks south
	public void testLocation10BlocksSouth() throws CustomerException {
		final int blocks = -11;
		Customer customer = new DroneDeliveryCustomer(validName, validMobileNumber, zero, blocks);
	}
	
	@Test (expected = CustomerException.class)
	// Test that exception is thrown if delivery order is placed more than 10 blocks east
	public void testLocation10BlocksEast() throws CustomerException {
		final int blocks = 11;
		Customer customer = new DroneDeliveryCustomer(validName, validMobileNumber, blocks, zero);
	}
	
	@Test (expected = CustomerException.class)
	// Test that exception is thrown if delivery order is placed more than 10 blocks west
	public void testLocation10BlocksWest() throws CustomerException {
		final int blocks = -11;
		Customer customer = new DroneDeliveryCustomer(validName, validMobileNumber, blocks, zero);
	}
	
	@Test (expected = CustomerException.class)
	// Test that exception is thrown if name is only white space
	public void testNameWithWhiteSpace() throws CustomerException {
		final String whiteSpace = "   ";
		Customer customer = new DroneDeliveryCustomer(whiteSpace, validMobileNumber, distance1, distance2);
	}
	
	@Test (expected = CustomerException.class)
	// Test that exception is thrown if mobile number contains letters or chars other than numbers
	public void testMobNumberWithChars() throws CustomerException {
		final String invalidNumber = "0123456TYD";
		Customer customer = new DroneDeliveryCustomer(validName, invalidNumber, distance1, distance2);
	}
}
