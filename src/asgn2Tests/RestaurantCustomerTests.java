package asgn2Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import asgn2Customers.Customer;
import asgn2Customers.CustomerFactory;
import asgn2Exceptions.CustomerException;
import asgn2Exceptions.LogHandlerException;
import asgn2Exceptions.PizzaException;
import asgn2Restaurant.PizzaRestaurant;

/**
 * A class that that tests the methods relating to the handling of Customer objects in the asgn2Restaurant.PizzaRestaurant
 * class as well as processLog and resetDetails.
 * 
 * @author David Nguyen
 */
public class RestaurantCustomerTests {
	
	PizzaRestaurant restaurant = new PizzaRestaurant();
	
	@Before
	// Set up a pizza restaurant using a valid log file
	public void setupRestaurant() throws CustomerException, PizzaException, LogHandlerException {
		restaurant.processLog("./logs/20170101.txt");
	}
	
	// General tests
	
	@Test
	// Test that number of orders is returned correctly
	public void testGetNumOfCustomerOrders() {
		final int customers = 3;
		assertEquals(customers, restaurant.getNumCustomerOrders());
	}
	
	@Test
	// Test that the total distance is returned correctly. In this case the distance should be 15 for the log loaded under "restaurant"
	public void testGetDeliveryDistancePickUpOrder() {
		final double total = 15.0;
		assertTrue(total == restaurant.getTotalDeliveryDistance());
	}
	
	@Test (expected = IndexOutOfBoundsException.class)
	// Test that reset details resets the lists for customers. Test should get an exception as pulling getCustomer with out of range index
	public void testReset() throws CustomerException {
		final int firstCustomer = 0;
		restaurant.resetDetails();
		restaurant.getCustomerByIndex(firstCustomer);
	}
	
	// Testing get customer by index method
	
	@Test
	// Test that get customer works for first customer
	public void testGetCustomerByIndexFirstCustomer() throws CustomerException {
		final int firstCustomer = 0;
		Customer customer = (Customer) CustomerFactory.getCustomer("DVC", "Casey Jones", "0123456789", 5, 5);
		assertEquals(restaurant.getCustomerByIndex(firstCustomer), customer);
	}
	
	@Test
	// Test that get customer works for last customer
	public void testGetCustomerByIndexLastCustomer() throws CustomerException {
		final int lastCustomer = restaurant.getNumCustomerOrders() - 1;
		Customer customer = (Customer) CustomerFactory.getCustomer("PUC", "Oroku Saki", "0111222333", 0, 0);
		assertEquals(restaurant.getCustomerByIndex(lastCustomer), customer);
	}
	
	// Exception tests for get customer by index
	
	@Test (expected = CustomerException.class) 
	// Test that exception is thrown when index is less than 0
	public void testIndexBelowZero() throws CustomerException {
		final int index = -1;
		restaurant.getCustomerByIndex(index);
	}
	
	@Test (expected = CustomerException.class) 
	// Test that exception is thrown when index is greater than range
	public void testIndexOutOfRange() throws CustomerException {
		final int index = restaurant.getNumCustomerOrders() + 1;
		restaurant.getCustomerByIndex(index);
	}
	
	
}
