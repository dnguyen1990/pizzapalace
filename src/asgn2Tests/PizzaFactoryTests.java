package asgn2Tests;

import static org.junit.Assert.*;

import java.time.LocalTime;
import org.junit.Test;
import asgn2Exceptions.PizzaException;
import asgn2Pizzas.MargheritaPizza;
import asgn2Pizzas.MeatLoversPizza;
import asgn2Pizzas.Pizza;
import asgn2Pizzas.PizzaFactory;
import asgn2Pizzas.VegetarianPizza;

/** 
 * A class that tests the asgn2Pizzas.PizzaFactory class.
 * 
 * @author Kodee Lund 
 * 
 */
public class PizzaFactoryTests {
	// TO DO
	private static String meatLoversCode = "PZL";
	private static String margheritaCode = "PZM";
	private static String vegetarianCode = "PZV";
	private static LocalTime duringOpenTime = LocalTime.parse("19:00:00");
	private static LocalTime someTime = LocalTime.parse("19:30:00");
	private final int quantity = 1;
	
	@Test (expected = PizzaException.class)
	// Tests Max orders exception
	public void testPizzaQuantityExceeded() throws PizzaException {
		int exceededOrder = 12;
		Pizza pizza = (MeatLoversPizza) PizzaFactory.getPizza(meatLoversCode, exceededOrder, duringOpenTime, someTime);
	}
	
	@Test (expected = PizzaException.class)
	// Tests minimum orders exception
	public void testPizzaQuantityMinimum() throws PizzaException {
		int belowMinOrder = 0;
		Pizza pizza = (MeatLoversPizza) PizzaFactory.getPizza(meatLoversCode, belowMinOrder, duringOpenTime, someTime);
	}
	
	@Test
	// Tests MeatLovers code is correct
	public void testGetMeatloversPizza() throws PizzaException {
		Pizza pizza = (MeatLoversPizza) PizzaFactory.getPizza(meatLoversCode, quantity, duringOpenTime, someTime);
		assertTrue(pizza.getPizzaType().equals("Meatlovers"));
	}
	
	@Test
	// Tests Vegetarian code is correct
	public void testGetVegetarianPizza() throws PizzaException {
		Pizza pizza = (VegetarianPizza) PizzaFactory.getPizza(vegetarianCode, quantity, duringOpenTime, someTime);
		assertTrue(pizza.getPizzaType().equals("Vegetarian"));
	}
	
	@Test
	// Tests Margherita code is correct
	public void testGetMargheritaPizza() throws PizzaException {
		Pizza pizza = (MargheritaPizza) PizzaFactory.getPizza(margheritaCode, quantity, duringOpenTime, someTime);
		assertTrue(pizza.getPizzaType().equals("Margherita"));
	}
	
	@Test (expected = PizzaException.class)
	// Tests that exception is thrown if the pizza code is not one of the 3
	public void testExceptionWhenCodeMismatch() throws PizzaException {
		final String invalidCode = "POP";
		Pizza pizza = (Pizza) PizzaFactory.getPizza(invalidCode, quantity, duringOpenTime, someTime);
	}
}
