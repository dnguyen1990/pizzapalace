package asgn2Tests;

import asgn2Pizzas.MargheritaPizza;
import asgn2Pizzas.MeatLoversPizza;
import asgn2Pizzas.Pizza;
import asgn2Pizzas.PizzaFactory;
import asgn2Pizzas.PizzaTopping;
import asgn2Pizzas.VegetarianPizza;

import java.time.LocalTime;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import asgn2Exceptions.PizzaException;
/**
 * A class that that tests the asgn2Pizzas.MargheritaPizza, asgn2Pizzas.VegetarianPizza, asgn2Pizzas.MeatLoversPizza classes. 
 * Note that an instance of asgn2Pizzas.MeatLoversPizza should be used to test the functionality of the 
 * asgn2Pizzas.Pizza abstract class. 
 * 
 * @author Kodee Lund
 *
 */
public class PizzaTests {
	// TO DO
	private static String meatLoversCode = "PZL";
	private static String margheritaCode = "PZM";
	private static String vegetarianCode = "PZV";
	private static LocalTime afterClosingTime = LocalTime.parse("23:01:00");
	private static LocalTime afterClosingTimeDelivery = LocalTime.parse("23:10:00");
	private static LocalTime beforeOpenTime = LocalTime.parse("18:59:00");
	private static LocalTime duringOpenTime = LocalTime.parse("19:00:00");
	private static LocalTime afterOpenTime = LocalTime.parse("19:09:00");
	private static LocalTime overHourFromOpenTime = LocalTime.parse("20:01:00");
	private static LocalTime someTime = LocalTime.parse("19:30:00");
	private static LocalTime beforeClosingTime = LocalTime.parse("22:40:00");
	private final int baseQuantity = 1;
	private final int someQuantity = 5;
	private final double MeatLoversPrice = 12.00;
	
	Pizza testMeatLoversPizza, testMeatLoversPizzaHQ;
	Pizza testMargheritaPizza, testMargheritaPizzaHQ;
	Pizza testVegetarianPizza, testVegetarianPizzaHQ;
	
	@Before 
	public void setUpPizzas() throws PizzaException {
		// Valid orders
		testMeatLoversPizza = new MeatLoversPizza(baseQuantity, duringOpenTime, someTime);
		testMargheritaPizza = new MargheritaPizza(baseQuantity, duringOpenTime, someTime);
		testVegetarianPizza = new VegetarianPizza(baseQuantity, duringOpenTime, someTime);
		
		testMeatLoversPizzaHQ = new MeatLoversPizza(someQuantity, duringOpenTime, someTime);
		testMargheritaPizzaHQ = new MargheritaPizza(someQuantity, duringOpenTime, someTime);
		testVegetarianPizzaHQ = new VegetarianPizza(someQuantity, duringOpenTime, someTime);
	}
	
	// General Tests
	@Test (expected = PizzaException.class)
	// Test for when order exceeds the maximum 10 pizza limit
	public void testMaximumOrderExceeded() throws PizzaException{
		final int order = 12;
		Pizza pizza = new MeatLoversPizza(order, duringOpenTime, someTime);
	}
	@Test (expected = PizzaException.class)
	// Test for when order has 0 or less pizzas
	public void testMinimumOrderNotMet() throws PizzaException{
		final int order = -1;
		Pizza pizza = new MeatLoversPizza(order, duringOpenTime, someTime);
	}
	@Test (expected = PizzaException.class)
	// Test that delivery time is not before order time
	public void testOrderTimeIsAfterDeliverTime() throws PizzaException{
		Pizza pizza = new MeatLoversPizza(someQuantity, someTime, duringOpenTime);
	}
	
	//Tests for MeatLovers Pizzas
	@Test 
	// Test for Tomato Topping on MeatLovers Pizza
	public void testMeatLoversPizzaToppingTomato() throws PizzaException{
		assertTrue(testMeatLoversPizza.containsTopping(PizzaTopping.TOMATO));
	}
	@Test 
	// Test for Cheese Topping on MeatLovers Pizza
	public void testMeatLoversPizzaToppingCheese() throws PizzaException {
		assertTrue(testMeatLoversPizza.containsTopping(PizzaTopping.CHEESE));
	}
	@Test 
	// Test for Bacon Topping on MeatLovers Pizza
	public void testMeatLoversPizzaToppingBacon() throws PizzaException {
		assertTrue(testMeatLoversPizza.containsTopping(PizzaTopping.BACON));
	}
	@Test
	// Test for Pepperoni Topping on MeatLovers Pizza
	public void testMeatLoversPizzaToppingPepperoni() throws PizzaException {
		assertTrue(testMeatLoversPizza.containsTopping(PizzaTopping.PEPPERONI));
	}	
	@Test
	// Test for Salami Topping on MeatLovers Pizza
	public void testMeatLoversPizzaToppingSalami() throws PizzaException {
		assertTrue(testMeatLoversPizza.containsTopping(PizzaTopping.SALAMI));
	}
	@Test 
	// Test for Eggplant Topping on MeatLovers Pizza
	public void testMeatLoversPizzaToppingEggplant() throws PizzaException {
		assertTrue(!testMeatLoversPizza.containsTopping(PizzaTopping.EGGPLANT));
	}
	@Test 
	// Test for Mushroom Topping on MeatLovers Pizza
	public void testMeatLoversPizzaToppingMushroom() throws PizzaException {
		assertTrue(!testMeatLoversPizza.containsTopping(PizzaTopping.MUSHROOM));
	}
	@Test 
	// Test for Capsicum Topping on MeatLovers Pizza
	public void testMeatLoversPizzaToppingCapsicum() throws PizzaException {
		assertTrue(!testMeatLoversPizza.containsTopping(PizzaTopping.CAPSICUM));
	}
	@Test 
	//Test for Quantity of MeatLovers Pizza 
	public void testMeatLoversPizzaQuantity() throws PizzaException {
		assertEquals(5, testMeatLoversPizzaHQ.getQuantity());
	}
	@Test (expected = PizzaException.class)
	//Tests for Order Before Open Time 
	public void testOrderMeatLoverBeforeOpenTime() throws PizzaException{
		Pizza pizza = new MeatLoversPizza(baseQuantity, beforeOpenTime, someTime);
	}
	@Test 
	// Tests cost Per Pizza
	public void testCostPerPizzaMeatLovers() throws PizzaException {
		final double cost = 5.0;
		assertTrue(testMeatLoversPizza.getCostPerPizza() == cost);
	}
	@Test 
	// Tests Price Per Pizza
	public void testPricePerPizzaMeatLovers() throws PizzaException {
		final double price = 12.0;
		assertTrue(testMeatLoversPizza.getPricePerPizza() == price);
	}
	@Test 
	// Tests Order Cost Per Order
	public void testOrderCostPerOrderMeatLovers() throws PizzaException {
		final double price = 25.0;
		assertTrue(testMeatLoversPizzaHQ.getOrderCost() == price);
	}
	@Test 
	// Tests Order Profit Per order
	public void testOrderProfitPerOrderMeatLovers() throws PizzaException {
		final double profit = 35.0;
		assertTrue(testMeatLoversPizzaHQ.getOrderProfit() == profit);
	}
	@Test
	// Tests for Pizza Type
	public void testMeatLoversPizzaType() throws PizzaException{
		assertEquals("Meatlovers", testMeatLoversPizza.getPizzaType());
	}
	@Test (expected = PizzaException.class) 
	// Tests for Invalid Delivery Time (10+ mins after Order Time)
	public void testMeatLoversPizzaValidDeliveryTime() throws PizzaException {
		Pizza pizza = new MeatLoversPizza(baseQuantity, duringOpenTime, afterOpenTime);
	}
	@Test 
	// Tests for Valid Delivery Time
	public void testMeatLoversPizzaValidTime() throws PizzaException {
		Pizza pizza = new MeatLoversPizza(baseQuantity, duringOpenTime, someTime);
	}
	@Test (expected = PizzaException.class)
	// Tests for Order over an hour
	public void testMeatLoversPizzaOrderOverHour() throws PizzaException {
		Pizza pizza = new MeatLoversPizza(baseQuantity, duringOpenTime, overHourFromOpenTime);
	}
	@Test (expected = PizzaException.class)
	//Tests for Order after hours
	public void testMeatLoversPizzaOrderAfterHours() throws PizzaException {
		Pizza pizza = new MeatLoversPizza(baseQuantity, afterClosingTime, afterClosingTimeDelivery);
	}
	@Test 
	//Tests for Order delviered after closing time still works
	public void testMeatLoversPizzaDelvieredAfterHours() throws PizzaException {
		Pizza pizza = new MeatLoversPizza(baseQuantity, beforeClosingTime, afterClosingTimeDelivery);
	}
	
	//Tests for Margherita Pizzas
	
	@Test 
	// Test for Tomato Topping on Margherita Pizza
	public void testMargheritaPizzaToppingTomato() throws PizzaException{
		assertTrue(testMargheritaPizza.containsTopping(PizzaTopping.TOMATO));
	}
	@Test 
	// Test for Cheese Topping on Margherita Pizza
	public void testMargheritaPizzaToppingCheese() throws PizzaException {
		assertTrue(testMargheritaPizza.containsTopping(PizzaTopping.CHEESE));
	}
	@Test 
	// Test for Bacon Topping on Margherita Pizza
	public void testMargheritaPizzaToppingBacon() throws PizzaException {
		assertTrue(!testMargheritaPizza.containsTopping(PizzaTopping.BACON));
	}
	@Test
	// Test for Pepperoni Topping on Margherita Pizza
	public void testMargheritaPizzaToppingPepperoni() throws PizzaException {
		assertTrue(!testMargheritaPizza.containsTopping(PizzaTopping.PEPPERONI));
	}	
	@Test
	// Test for Salami Topping on Margherita Pizza
	public void testMargheritaPizzaToppingSalami() throws PizzaException {
		assertTrue(!testMargheritaPizza.containsTopping(PizzaTopping.SALAMI));
	}
	@Test 
	// Test for Eggplant Topping on Margherita Pizza
	public void testMargheritaPizzaToppingEggplant() throws PizzaException {
		assertTrue(!testMargheritaPizza.containsTopping(PizzaTopping.EGGPLANT));
	}
	@Test 
	// Test for Mushroom Topping on Margherita Pizza
	public void testMargheritaPizzaToppingMushroom() throws PizzaException {
		assertTrue(!testMargheritaPizza.containsTopping(PizzaTopping.MUSHROOM));
	}
	@Test 
	// Test for Capsicum Topping on Margherita Pizza
	public void testMargheritaPizzaToppingCapsicum() throws PizzaException {
		assertTrue(!testMargheritaPizza.containsTopping(PizzaTopping.CAPSICUM));
	}
	@Test 
	//Test for Quantity of Margherita Pizza 
	public void testMargheritaPizzaQuantity() throws PizzaException {
		assertEquals(5, testMargheritaPizzaHQ.getQuantity());
	}
	@Test (expected = PizzaException.class)
	//Tests for Order Before Open Time 
	public void testOrderMargheritaBeforeOpenTime() throws PizzaException{
		Pizza pizza = new MargheritaPizza(baseQuantity, beforeOpenTime, someTime);
	}
	@Test 
	// Tests cost Per Pizza
	public void testCostPerPizzaMargherita() throws PizzaException {
		final double cost = 1.5;
		assertTrue(testMargheritaPizza.getCostPerPizza() == cost);
	}
	@Test 
	// Tests Price Per Pizza
	public void testPricePerPizzaMargherita() throws PizzaException {
		final double price = 8.0;
		assertTrue(testMargheritaPizza.getPricePerPizza() == price);
	}
	@Test 
	// Tests Order Cost Per Order
	public void testOrderCostPerOrderMargherita() throws PizzaException {
		final double price = 7.5;
		assertTrue(testMargheritaPizzaHQ.getOrderCost() == price);
	}
	@Test 
	// Tests Order Profit Per order
	public void testOrderProfitPerOrderMargherita() throws PizzaException {
		final double profit = 32.5;
		assertTrue(testMargheritaPizzaHQ.getOrderProfit() == profit);
	}
	@Test
	// Tests for Pizza Type
	public void testMargheritaPizzaType() throws PizzaException{
		assertEquals("Margherita", testMargheritaPizza.getPizzaType());
	}
	@Test (expected = PizzaException.class) 
	// Tests for Invalid Delivery Time (10+ mins after Order Time)
	public void testMargheritaPizzaValidDeliveryTime() throws PizzaException {
		Pizza pizza = new MargheritaPizza(baseQuantity, duringOpenTime, afterOpenTime);
	}
	@Test 
	// Tests for Valid Delivery Time
	public void testMargheritaPizzaValidTime() throws PizzaException {
		Pizza pizza = new MargheritaPizza(baseQuantity, duringOpenTime, someTime);
	}
	@Test (expected = PizzaException.class)
	// Tests for Order over an hour
	public void testMargheritaPizzaOrderOverHour() throws PizzaException {
		Pizza pizza = new MargheritaPizza(baseQuantity, duringOpenTime, overHourFromOpenTime);
	}
	@Test (expected = PizzaException.class)
	//Tests for Order after hours
	public void testMargheritaPizzaOrderAfterHours() throws PizzaException {
		Pizza pizza = new MargheritaPizza(baseQuantity, afterClosingTime, afterClosingTimeDelivery);
	}
	@Test 
	//Tests for Order delviered after closing time still works
	public void testMargheritaPizzaDelvieredAfterHours() throws PizzaException {
		Pizza pizza = new MargheritaPizza(baseQuantity, beforeClosingTime, afterClosingTimeDelivery);
	}
	
	//Tests for Vegetarian Pizzas
	
	@Test 
	// Test for Tomato Topping on Vegetarian Pizza
	public void testVegetarianPizzaToppingTomato() throws PizzaException{
		assertTrue(testVegetarianPizza.containsTopping(PizzaTopping.TOMATO));
	}
	@Test 
	// Test for Cheese Topping on Vegetarian Pizza
	public void testVegetarianPizzaToppingCheese() throws PizzaException {
		assertTrue(testVegetarianPizza.containsTopping(PizzaTopping.CHEESE));
	}
	@Test 
	// Test for Eggplant Topping on Vegetarian Pizza
	public void testVegetarianPizzaToppingEggplant() throws PizzaException {
		assertTrue(testVegetarianPizza.containsTopping(PizzaTopping.EGGPLANT));
	}
	@Test 
	// Test for Mushroom Topping on Vegetarian Pizza
	public void testVegetarianPizzaToppingMushroom() throws PizzaException {
		assertTrue(testVegetarianPizza.containsTopping(PizzaTopping.MUSHROOM));
	}
	@Test 
	// Test for Capsicum Topping on Vegetarian Pizza
	public void testVegetarianPizzaToppingCapsicum() throws PizzaException {
		assertTrue(testVegetarianPizza.containsTopping(PizzaTopping.CAPSICUM));
	}
	@Test 
	// Test for Bacon Topping on Vegetarian Pizza
	public void testVegetarianPizzaToppingBacon() throws PizzaException {
		assertTrue(!testVegetarianPizza.containsTopping(PizzaTopping.BACON));
	}
	@Test
	// Test for Pepperoni Topping on Vegetarian Pizza
	public void testVegetarianPizzaToppingPepperoni() throws PizzaException {
		assertTrue(!testVegetarianPizza.containsTopping(PizzaTopping.PEPPERONI));
	}	
	@Test
	// Test for Salami Topping on Vegetarian Pizza
	public void testVegetarianPizzaToppingSalami() throws PizzaException {
		assertTrue(!testVegetarianPizza.containsTopping(PizzaTopping.SALAMI));
	}
	@Test 
	//Test for Quantity of Vegetarian Pizza 
	public void testVegetarianPizzaQuantity() throws PizzaException {
		assertEquals(5, testVegetarianPizzaHQ.getQuantity());
	}
	@Test (expected = PizzaException.class)
	//Tests for Order Before Open Time 
	public void testOrderVegetarianBeforeOpenTime() throws PizzaException{
		Pizza pizza = new VegetarianPizza(baseQuantity, beforeOpenTime, someTime);
	}
	@Test 
	// Tests cost Per Pizza
	public void testCostPerPizzaVegetarian() throws PizzaException {
		final double cost = 5.5;
		assertTrue(testVegetarianPizza.getCostPerPizza() == cost);
	}
	@Test 
	// Tests Price Per Pizza
	public void testPricePerPizzaVegetarian() throws PizzaException {
		final double price = 10.0;
		assertTrue(testVegetarianPizza.getPricePerPizza() == price);
	}
	@Test 
	// Tests Order Cost Per Order
	public void testOrderCostPerOrderVegetarian() throws PizzaException {
		final double price = 27.5;
		assertTrue(testVegetarianPizzaHQ.getOrderCost() == price);
	}
	@Test 
	// Tests Order Profit Per order
	public void testOrderProfitPerOrderVegetarian() throws PizzaException {
		final double profit = 22.5;
		assertTrue(testVegetarianPizzaHQ.getOrderProfit() == profit);
	}
	@Test
	// Tests for Pizza Type
	public void testVegetarianPizzaType() throws PizzaException{
		assertEquals("Vegetarian", testVegetarianPizza.getPizzaType());
	}
	@Test (expected = PizzaException.class) 
	// Tests for Invalid Delivery Time (10+ mins after Order Time)
	public void testVegetarianPizzaValidDeliveryTime() throws PizzaException {
		Pizza pizza = new VegetarianPizza(baseQuantity, duringOpenTime, afterOpenTime);
	}
	@Test 
	// Tests for Valid Delivery Time
	public void testVegetarianPizzaValidTime() throws PizzaException {
		Pizza pizza = new VegetarianPizza(baseQuantity, duringOpenTime, someTime);
	}
	@Test (expected = PizzaException.class)
	// Tests for Order over an hour
	public void testVegetariansPizzaOrderOverHour() throws PizzaException {
		Pizza pizza = new VegetarianPizza(baseQuantity, duringOpenTime, overHourFromOpenTime);
	}
	@Test (expected = PizzaException.class)
	//Tests for Order after hours
	public void testVegetarianPizzaOrderAfterHours() throws PizzaException {
		Pizza pizza = new VegetarianPizza(baseQuantity, afterClosingTime, afterClosingTimeDelivery);	
	}
	@Test 
	//Tests for Order delviered after closing time still works
	public void testVegetarianPizzaDelvieredAfterHours() throws PizzaException {
		Pizza pizza = new VegetarianPizza(baseQuantity, beforeClosingTime, afterClosingTimeDelivery);
	}
}	

