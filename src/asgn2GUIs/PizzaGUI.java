package asgn2GUIs;

import java.awt.event.ActionEvent;


import java.awt.event.ActionListener;
import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.swing.text.DefaultCaret;

import asgn2Customers.Customer;
import asgn2Exceptions.CustomerException;
import asgn2Exceptions.LogHandlerException;
import asgn2Exceptions.PizzaException;
import asgn2Pizzas.Pizza;
import asgn2Restaurant.PizzaRestaurant;

import java.awt.*;
import javax.swing.*;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;


/**
 * 
 * This class is the graphical user interface for the rest of the system.
 * It currently is working 100%. Users can load log files then click another button to display the information from the files
 * in table format. Daily calculations can be done at any time after a log file is loaded successfully. Finally users can reset the 
 * data displayed by clicking on the reset buton at any time. This will lead the user back to the initial view and users can then load
 * a new log file. If any errors occur from reading a log file, the user will be alerted with a message box which displays the line 
 * causing the error as well as a description of why.
 * 
 * @author David Nguyen and Kodee Lund
 *
 */
public class PizzaGUI extends javax.swing.JFrame implements Runnable, ActionListener {
	// Dimensions for GUI
	private static final int WIDTH = 1200;
	private static final int HEIGHT = 620;
	private static final int PIZZA_TABLE_WIDTH = 450;
	private static final int CUSTOMER_TABLE_WIDTH = 700;
	private static final int TABLE_HEIGHT = 480;
	private static final int PIZZA_TABLE_LABEL = 400;
	private static final int CUSTOMER_TABLE_LABEL = 700;
	private static final int FIELD_HEIGHT = 20;
	private static final int PANEL_HEIGHT = 40;
	private static final int TEXT_WIDTH = 800;
	private static final int BUTTON_WIDTH = 200;
	private static final int BUTTON_HEIGHT = 30;
	
	// Panels for GUI
	private JPanel panelNorth;
	private JPanel panelEast;
	private JPanel panelWest;
	private JPanel panelCenter;
	private JPanel panelButtons;
	
	// Buttons for GUI
	private JButton btnLoad;
	private JButton btnCalculate;
	private JButton btnDisplay;
	private JButton btnReset;
	
	// Variables for daily profit and distance
	private String dailyProfit;
	private String dailyDistance;
	
	// Two tables, one for pizza and one for customer both used in a scroll pane
	private JTable pizzaDisplay;
	private JTable customerDisplay;
	private JScrollPane customerScrollPane;
	private JScrollPane pizzaScrollPane;
	
	// Label to show the file path of log
	private JLabel filePath = new JLabel("");
	
	// File chooser to select a file
	final JFileChooser fileChooser = new JFileChooser();
	
	private PizzaRestaurant pizzaRestaurant;
	private Object[][] pizzaOrders;
	private Object[][] customerData;
	private String[] pizzaColumnTitle = new String[] {
			"Pizza", "Quantity", "Order Price ($)", "Order Cost ($)", "Order Profit ($)"
    };
	private String[] customerColumnTitle = new String[] {
			"Customer Name", "Mobile No.", "Customer Type", "X location", "Y Location", "Delivery Distance"
    };
	
	// Used to convert type double variables to 2 decimal place format
	private DecimalFormat decimalFormat = new DecimalFormat("#0.00");
	
	/**
	 * Creates a new Pizza GUI with the specified title 
	 * @param title - The title for the supertype JFrame
	 */
	public PizzaGUI(String title) {
		super(title);
	}
	
	// Method to create GUI
	private void createGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
		setSize(WIDTH, HEIGHT);
		setResizable(false);
		pizzaRestaurant = new PizzaRestaurant();
		panelNorth = createPanel(Color.gray);
		panelCenter = createPanel(Color.white);
		panelEast = createPanel(Color.gray);
		panelWest = createPanel(Color.gray);
		panelButtons = createPanel(Color.gray);
		panelNorth.setPreferredSize(new Dimension(WIDTH, PANEL_HEIGHT));
		panelButtons.setPreferredSize(new Dimension(WIDTH, PANEL_HEIGHT));
		
		btnLoad = createAButton("Load A File");
		btnCalculate = createAButton("Total Daily Stats");
		btnDisplay = createAButton("Display Info");
		btnReset = createAButton("Reset");
		
		layoutButtonPanel();
		northPanelLayout();
		
		// Orientation for each panel
		this.getContentPane().add(panelCenter, BorderLayout.CENTER);
		this.getContentPane().add(panelEast, BorderLayout.EAST);
		this.getContentPane().add(panelNorth, BorderLayout.NORTH);
		this.getContentPane().add(panelWest, BorderLayout.WEST);
		this.getContentPane().add(panelButtons, BorderLayout.SOUTH);
		repaint();
		this.setVisible(true);
	}
	
	// Setup of central panel which contains the two tables of data and scroll panes
	private void centerPanelLayout() {
		TableModel tablePizza = new DefaultTableModel(pizzaOrders, pizzaColumnTitle);
		TableModel tableCustomer = new DefaultTableModel(customerData, customerColumnTitle);
        customerDisplay = new JTable(tableCustomer);
        pizzaDisplay = new JTable(tablePizza);
        customerScrollPane = new JScrollPane(customerDisplay);
        pizzaScrollPane = new JScrollPane(pizzaDisplay);
        customerScrollPane.setPreferredSize(new Dimension(CUSTOMER_TABLE_WIDTH, TABLE_HEIGHT));
        pizzaScrollPane.setPreferredSize(new Dimension(PIZZA_TABLE_WIDTH, TABLE_HEIGHT));
        JLabel pizzaOrderLabel = new JLabel("Pizza Order List");
        pizzaOrderLabel.setPreferredSize(new Dimension(PIZZA_TABLE_LABEL, FIELD_HEIGHT));
        JLabel customerListLabel = new JLabel("List of Customers");
        customerListLabel.setPreferredSize(new Dimension(CUSTOMER_TABLE_LABEL, FIELD_HEIGHT));
        panelCenter.add(customerListLabel);
        panelCenter.add(pizzaOrderLabel);
		panelCenter.add(customerScrollPane);
		panelCenter.add(pizzaScrollPane);
	}
	
	// Calculates the daily profits and total distance
	private void performCalculation() {
		dailyDistance = decimalFormat.format(pizzaRestaurant.getTotalDeliveryDistance());
		dailyProfit = decimalFormat.format(pizzaRestaurant.getTotalProfit());
	}

	// Setup the layout of the top panel which contains the file path
	private void northPanelLayout() {
		panelNorth.add(filePath);
		panelNorth.setPreferredSize(new Dimension(TEXT_WIDTH, FIELD_HEIGHT));
		filePath.setForeground(Color.WHITE);
	}
	
	// Setup the layout for button panel
	private void layoutButtonPanel() {
		GridBagLayout layout = new GridBagLayout();
		panelButtons.setLayout(layout);
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.NONE;
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.weightx = 100;
		constraints.weighty = 100;
		
		// Add all buttons to the button panel
		addObjectToPanel(panelButtons, btnLoad, constraints, 0, 0, 1, 2);
		addObjectToPanel(panelButtons, btnDisplay, constraints, 3, 0, 1, 2);
		addObjectToPanel(panelButtons, btnCalculate, constraints, 6, 0, 1, 2);
		addObjectToPanel(panelButtons, btnReset, constraints, 9, 0, 1, 2);
		
		// Sets the following buttons to be disabled by default
		btnDisplay.setEnabled(false);
		btnCalculate.setEnabled(false);
		btnReset.setEnabled(false);
	}
	
	// Method to create a panel
	private JPanel createPanel(Color colour) {
		JPanel panel = new JPanel();
		panel.setBackground(colour);
		return panel;
	}
	
	// Method to create a button
	private JButton createAButton(String text) {
		JButton button = new JButton(text);
		button.addActionListener(this);
		button.setPreferredSize(new Dimension(BUTTON_WIDTH, BUTTON_HEIGHT));
		return button;
	}
	
	// Method to add a component to a panel
	private void addObjectToPanel(JPanel panel, Component component, GridBagConstraints constraint, int xPos, int yPos, int width, int height) {
		constraint.gridx = xPos;
		constraint.gridy = yPos;
		constraint.gridheight = height;
		constraint.gridwidth = width;
		panel.add(component, constraint);
	}
	
	// Creates a 2D array of pizza data
	private void createPizzaData() {
		final int columns = 5;
		pizzaOrders = new Object[pizzaRestaurant.getNumPizzaOrders()][columns];
		try {
			// For each column (0 - 4) insert the relevant data
			for (int i = 0; i < pizzaRestaurant.getNumPizzaOrders(); i++) {
				pizzaOrders[i][0] = pizzaRestaurant.getPizzaByIndex(i).getPizzaType();
				pizzaOrders[i][1] = pizzaRestaurant.getPizzaByIndex(i).getQuantity();
				pizzaOrders[i][2] = decimalFormat.format(pizzaRestaurant.getPizzaByIndex(i).getOrderPrice());
				pizzaOrders[i][3] = decimalFormat.format(pizzaRestaurant.getPizzaByIndex(i).getOrderCost());
				pizzaOrders[i][4] = decimalFormat.format(pizzaRestaurant.getPizzaByIndex(i).getOrderProfit());
			}
		} catch (PizzaException e) {
			e.printStackTrace();
		}
	}
	
	// Creates a 2D array of customer data
	private void createCustomerData() {
		final int columns = 6;
		customerData = new Object[pizzaRestaurant.getNumCustomerOrders()][columns];
		try {
			// For each column (0 - 5) insert the relevant data
			for (int i = 0; i < pizzaRestaurant.getNumCustomerOrders(); i++) {
				customerData[i][0] = pizzaRestaurant.getCustomerByIndex(i).getName();
				customerData[i][1] = pizzaRestaurant.getCustomerByIndex(i).getMobileNumber();
				customerData[i][2] = pizzaRestaurant.getCustomerByIndex(i).getCustomerType();
				customerData[i][3] = pizzaRestaurant.getCustomerByIndex(i).getLocationX();
				customerData[i][4] = pizzaRestaurant.getCustomerByIndex(i).getLocationY();
				customerData[i][5] = decimalFormat.format(pizzaRestaurant.getCustomerByIndex(i).getDeliveryDistance());
			}
		} catch (CustomerException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void run() {
		createGUI();
	}

	@Override
	// Interacting with GUI
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		// Performs when user clicks on the "Load" button
		if (source == btnLoad) {
			int returnValue = fileChooser.showOpenDialog(this);
			if (returnValue == JFileChooser.APPROVE_OPTION) {
				File logFile = fileChooser.getSelectedFile();
				filePath.setText("Loading From File: " + logFile.getAbsolutePath());
				try {
					pizzaRestaurant.processLog(logFile.getAbsolutePath());
					createPizzaData();
					createCustomerData();
					JOptionPane.showMessageDialog(null, "File has successfully been loaded and ready to display.");
					btnDisplay.setEnabled(true);
					btnCalculate.setEnabled(true);
					btnReset.setEnabled(true);
					btnLoad.setEnabled(false);
				} catch (Exception exception) {
					// If any exceptions/errors then do
					JOptionPane.showMessageDialog(null, exception.getMessage());
					JLabel errorMessage = new JLabel("Error loading from file: " + logFile.getAbsolutePath() + ". Please click reset and try loading another file.");
					panelCenter.add(errorMessage);
					repaint();
					this.setVisible(true);
					btnDisplay.setEnabled(false);
					btnCalculate.setEnabled(false);
					btnLoad.setEnabled(false);
					btnReset.setEnabled(true);
				}			
			} 
		}
		
		// Display info button is only enabled after log is loaded. Clicking "Display Info" will show the list of customers and list of pizza orders.
		if (source == btnDisplay) {
			centerPanelLayout();
			btnCalculate.setEnabled(true);
			btnDisplay.setEnabled(false);
			repaint();
			this.setVisible(true);
		}
		
		// Performing calculations can be done after log is loaded. When clicked, a message box will show daily stats.
		if (source == btnCalculate) {
			performCalculation();
			JOptionPane.showMessageDialog(null, "Today's stats are as follow:\nTotal Profit = $" + dailyProfit + "\nTotal Distance = " + dailyDistance + " blocks");
		}
		
		// Reset button. When clicked all data is cleared from the main panel and file path is reset.
		if (source == btnReset) {
			int reset = JOptionPane.showConfirmDialog(null, "Are you sure you wish to reset data?");
			if (reset == JOptionPane.YES_OPTION) {
				pizzaRestaurant.resetDetails();
				btnDisplay.setEnabled(false);
				btnCalculate.setEnabled(false);
				btnReset.setEnabled(false);
				btnLoad.setEnabled(true);
				filePath.setText("");
				customerData = new Object[0][0];
				pizzaOrders = new Object[0][0];
				panelCenter.removeAll();
				repaint();
				this.setVisible(true);
			}
		}
	}

}
