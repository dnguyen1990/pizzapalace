package asgn2Pizzas;

import java.time.LocalTime;

import asgn2Exceptions.PizzaException;


/**
 * An abstract class that represents pizzas sold at the Pizza Palace restaurant. 
 * The Pizza class is used as a base class of VegetarianPizza, MargheritaPizza and MeatLoversPizza. 
 * Each of these subclasses have a different set of toppings. A description of the class's fields
 * and their constraints is provided in Section 5.1 of the Assignment Specification. 
 * 
 * @author David Nguyen
 *
 */
public abstract class Pizza  {
	
	private int numOfPizzas;
	private String pizzaType;
	private double pizzaPrice;
	private double costOfPizza;
	
	/**
	 *  This class represents a pizza produced at the Pizza Palace restaurant.  A detailed description of the class's fields
	 *  and parameters is provided in the Assignment Specification, in particular in Section 5.1. 
	 *  A PizzaException is thrown if the any of the constraints listed in Section 5.1 of the Assignment Specification
	 *  are violated. 
     *
     *  PRE: TRUE
	 *  POST: All field values except cost per pizza are set
	 * 
	 * @param quantity - The number of pizzas ordered 
	 * @param orderTime - The time that the pizza order was made and sent to the kitchen 
	 * @param deliveryTime - The time that the pizza was delivered to the customer
	 * @param type -  A human understandable description of this Pizza type
	 * @param price - The price that the pizza is sold to the customer
	 * @throws PizzaException if number of pizzas ordered exceeds maximum or does not meet minimum; if none of the pizza codes match;
	 * if order is made after kitchen is closed, order is made before kitchen opens, pizza is made in less than 10 minutes;
	 * if order time or delivery time is invalid, pizza is over an hour old, delivery time is before order time.
	 */
	public Pizza(int quantity, LocalTime orderTime, LocalTime deliveryTime, String type, double price) throws PizzaException{	
		final int closingHour = 23; // represents 24 hour clock for 11pm
		final int openingHour = 19; // represents 24 hour clock for 7pm
		final int twentyFourHour = 24;
		final int sixtyMinutes = 60;
		final int sixtySeconds = 60;
		final int maxOrder = 10;
		final int minOrder = 1;
		if (quantity > maxOrder) { 
			throw new PizzaException("Maximum number of pizzas ordered exceeded");
		} else if (quantity < minOrder) {
			throw new PizzaException("Minimum number of pizzas ordered has not been met");
		} else if (!type.equals("Meatlovers") && !type.equals("Margherita") && !type.equals("Vegetarian")) {
			throw new PizzaException("Invalid pizza type");
		} else if (orderTime.getHour() >= closingHour) {
			throw new PizzaException("Kitchen has closed");
		} else if (orderTime.getHour() < openingHour) {
			throw new PizzaException("Kitchen is not open yet");
		} else if (orderTime.getHour() > twentyFourHour || orderTime.getHour() < 0 || orderTime.getMinute() >= sixtyMinutes || 
				orderTime.getMinute() < 0 || orderTime.getSecond() < 0 || orderTime.getSecond() >= sixtySeconds) {
			throw new PizzaException("Order time is invalid");
		} else if (deliveryTime.getHour() > twentyFourHour || deliveryTime.getHour() < 0 || deliveryTime.getMinute() >= sixtyMinutes || 
				deliveryTime.getMinute() < 0 || deliveryTime.getSecond() < 0 || deliveryTime.getSecond() >= sixtySeconds) {
			throw new PizzaException("Delivery time is invalid");
		} else if (checkIfPizzaIsOverHour(orderTime, deliveryTime)) {
			throw new PizzaException("Delivery time is over an hour from order time. Discard pizza"); 
		} else if (checkIfDeliveryTimeIsBeforeOrderTime(orderTime, deliveryTime)) {
			throw new PizzaException("Invalid times. Delivery time is before order time"); 
		} else if (!checkIfPizzaIsDeliveredAfter10Minutes(orderTime, deliveryTime)) {
			throw new PizzaException("Invalid times. Minimum cooking time for pizza is 10 minutes."); 
		} else {
			this.pizzaPrice = price;
			this.pizzaType = type;
			this.numOfPizzas = quantity;
		}
	}
	
	// Method converts the order and delivery times to seconds then checks if there is at least a difference of 10 minutes
	private boolean checkIfPizzaIsDeliveredAfter10Minutes(LocalTime orderTime, LocalTime deliveryTime) {
		final int secondsInMinutes = 60;
		final int secondsInHour = 60 * secondsInMinutes;
		final int minTime = 10 * secondsInMinutes;
		int orderTimeInSec = orderTime.getHour() * secondsInHour + orderTime.getMinute() * secondsInMinutes + orderTime.getSecond();
		int deliveryTimeInSec = deliveryTime.getHour() * secondsInHour + deliveryTime.getMinute() * secondsInMinutes + deliveryTime.getSecond();
		if ((deliveryTimeInSec - orderTimeInSec) >= minTime) {
			return true;
		} else {
			return false;
		}
	}
	
	// Method converts the order and delivery times to seconds then checks order time comes before delivery time
	private boolean checkIfDeliveryTimeIsBeforeOrderTime(LocalTime orderTime, LocalTime deliveryTime) {
		final int secondsInMinutes = 60;
		final int secondsInHour = 60 * secondsInMinutes;
		int orderTimeInSec = orderTime.getHour() * secondsInHour + orderTime.getMinute() * secondsInMinutes + orderTime.getSecond();
		int deliveryTimeInSec = deliveryTime.getHour() * secondsInHour + deliveryTime.getMinute() * secondsInMinutes + deliveryTime.getSecond();
		if (deliveryTimeInSec <= orderTimeInSec) {
			return true;
		} else {
			return false;
		}
	}
	
	// Method converts the order and delivery times to seconds then checks if the difference is over an hour
	private boolean checkIfPizzaIsOverHour(LocalTime orderTime, LocalTime deliveryTime) {
		final int secondsInMinutes = 60;
		final int secondsInHour = 60 * secondsInMinutes;
		int orderTimeInSec = orderTime.getHour() * secondsInHour + orderTime.getMinute() * secondsInMinutes + orderTime.getSecond();
		int deliveryTimeInSec = deliveryTime.getHour() * secondsInHour + deliveryTime.getMinute() * secondsInMinutes + deliveryTime.getSecond();
		int timeDifference = deliveryTimeInSec - orderTimeInSec;
		if (timeDifference > secondsInHour) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Calculates how much a pizza would cost to make calculated from its toppings.
	 *  
     * <P> PRE: TRUE
	 * <P> POST: The cost field is set to sum of the Pizzas's toppings
	 */
	public final void calculateCostPerPizza(){
		double cheese = PizzaTopping.CHEESE.getCost();
		double tomato = PizzaTopping.TOMATO.getCost();
		double bacon = PizzaTopping.BACON.getCost();
		double salami = PizzaTopping.SALAMI.getCost();
		double pepperoni = PizzaTopping.PEPPERONI.getCost();
		double capsicum = PizzaTopping.CAPSICUM.getCost();
		double mushroom = PizzaTopping.MUSHROOM.getCost();
		double eggplant = PizzaTopping.EGGPLANT.getCost();
		if (this.pizzaType.equals("Meatlovers")) {
			this.costOfPizza = tomato + cheese + bacon + pepperoni + salami;
		} else if (this.pizzaType.equals("Margherita")) {
			this.costOfPizza = tomato + cheese;
		} else if (this.pizzaType.equals("Vegetarian")) {
			this.costOfPizza = tomato + cheese + eggplant + mushroom + capsicum;
		} 
	}
	
	/**
	 * Returns the amount that an individual pizza costs to make.
	 * @return The amount that an individual pizza costs to make.
	 */
	public final double getCostPerPizza(){
		this.calculateCostPerPizza();
		return this.costOfPizza;
	}

	/**
	 * Returns the amount that an individual pizza is sold to the customer.
	 * @return The amount that an individual pizza is sold to the customer.
	 */
	public final double getPricePerPizza(){
		return this.pizzaPrice;
	}

	/**
	 * Returns the amount that the entire order costs to make, taking into account the type and quantity of pizzas. 
	 * @return The amount that the entire order costs to make, taking into account the type and quantity of pizzas. 
	 */
	public final double getOrderCost(){
		return this.getCostPerPizza() * this.getQuantity();
	}
	
	/**
	 * Returns the amount that the entire order is sold to the customer, taking into account the type and quantity of pizzas. 
	 * @return The amount that the entire order is sold to the customer, taking into account the type and quantity of pizzas. 
	 */
	public final double getOrderPrice(){
		return this.getPricePerPizza() * this.getQuantity();
	}
	
	
	/**
	 * Returns the profit made by the restaurant on the order which is the order price minus the order cost. 
	 * @return  Returns the profit made by the restaurant on the order which is the order price minus the order cost.
	 */
	public final double getOrderProfit(){
		return this.getOrderPrice() - this.getOrderCost();
	}
	

	/**
	 * Indicates if the pizza contains the specified pizza topping or not. 
	 * @param topping -  A topping as specified in the enumeration PizzaTopping
	 * @return Returns  true if the instance of Pizza contains the specified topping and false otherwise.
	 */
	public final boolean containsTopping(PizzaTopping topping){
		boolean containsTopping = false;
		if (this.pizzaType == "Margherita") {
			if (PizzaTopping.CHEESE.equals(topping) || PizzaTopping.TOMATO.equals(topping)) {
				containsTopping = true;
			}
		}
		if (this.pizzaType == "Meatlovers") {
			if (PizzaTopping.BACON.equals(topping) || PizzaTopping.SALAMI.equals(topping) || PizzaTopping.PEPPERONI.equals(topping) || PizzaTopping.CHEESE.equals(topping) || PizzaTopping.TOMATO.equals(topping)) {
				containsTopping = true;
			}
		} 
		if (this.pizzaType == "Vegetarian") {
			if (PizzaTopping.EGGPLANT.equals(topping) || PizzaTopping.MUSHROOM.equals(topping) || PizzaTopping.CAPSICUM.equals(topping) || PizzaTopping.CHEESE.equals(topping) || PizzaTopping.TOMATO.equals(topping)) {
				containsTopping = true;
			}
		}
		return containsTopping;
	}
	
	/**
	 * Returns the quantity of pizzas ordered. 
	 * @return the quantity of pizzas ordered. 
	 */
	public final int getQuantity(){
		return this.numOfPizzas;
	}

	/**
	 * Returns a human understandable description of the Pizza's type. 
	 * The valid alternatives are listed in Section 5.1 of the Assignment Specification. 
	 * @return A human understandable description of the Pizza's type.
	 */
	public final String getPizzaType(){
		return this.pizzaType;
	}


	/**
	 * Compares *this* Pizza object with an instance of an *other* Pizza object and returns true if  
	 * if the two objects are equivalent, that is, if the values exposed by public methods are equal.
	 * You do not need to test this method.
	 *  
	 * @return true if *this* Pizza object and the *other* Pizza object have the same values returned for 	
	 * getCostPerPizza(), getOrderCost(), getOrderPrice(), getOrderProfit(), getPizzaType(), getPricePerPizza() 
	 * and getQuantity().
	 *   
	 */
	@Override
	public boolean equals(Object other){
		Pizza otherPizza = (Pizza) other;
		return ((this.getCostPerPizza()) == (otherPizza.getCostPerPizza()) &&
			(this.getOrderCost()) == (otherPizza.getOrderCost())) &&				
			(this.getOrderPrice()) == (otherPizza.getOrderPrice()) &&
			(this.getOrderProfit()) == (otherPizza.getOrderProfit()) &&
			(this.getPizzaType() == (otherPizza.getPizzaType()) &&
			(this.getPricePerPizza()) == (otherPizza.getPricePerPizza()) &&
			(this.getQuantity()) == (otherPizza.getQuantity()));
	}

	
}
